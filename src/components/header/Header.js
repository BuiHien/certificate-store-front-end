import React from 'react';
import './Header.css';
import netcompany from '../../netcompany.svg';

function Header() {
    return (
        <div>
            <div className="collapse navbar-collapse header" id="hwvNavbar">
                <img src={netcompany} width="25%" height="50" />
                <div className="header-menu-right">
                    <ul className="nav navbar-nav navbar-right header-ul">
                        <li className="header-li"><a>ABOUT US</a></li>
                    </ul>
                </div>
            </div>
            <div className="header-shadow"></div>
            <div className="clearfix-div"></div>
        </div>
    );
}

export default Header;
