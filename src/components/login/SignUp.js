import React from 'react';
import './Login.css';
import { useHistory } from 'react-router-dom';
import netcompany from "../../netcompany.svg";

function SignUp() {
    const history = useHistory();

    const handleClick = () => {
        history.push("/login");
    }
    return (
        <div>
            <div className="collapse navbar-collapse header" id="hwvNavbar">
                <img src={netcompany} width="25%" height="50" />
                <div className="header-menu-right">
                    <ul className="nav navbar-nav navbar-right header-ul">
                        <li className="header-li"><a>ABOUT US</a></li>
                    </ul>
                </div>
            </div>
            <div className="header-shadow"></div>
            <div className="clearfix-div"></div>
            <div className="sign-in-form">
                <form>
                    <h3>Sign Up</h3>

                    <div className="form-group">
                        <label>First name</label>
                        <input type="text" className="form-control" placeholder="First name" />
                    </div>

                    <div className="form-group">
                        <label>Last name</label>
                        <input type="text" className="form-control" placeholder="Last name" />
                    </div>

                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email" className="form-control" placeholder="Enter email" />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Enter password" />
                    </div>

                    <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                    <p className="forgot-password text-right">
                        Already registered <a onClick={handleClick} type="button"> sign in? </a>
                    </p>
                </form>
            </div>
        </div>
    );
}

export default SignUp;
