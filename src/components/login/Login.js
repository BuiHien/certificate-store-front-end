import React from 'react';
import './Login.css';
import netcompany from '../../netcompany.svg';
import header from '../header/Header';

function Login() {
    return (
        <div>
            <div className="collapse navbar-collapse header" id="hwvNavbar">
                <img src={netcompany} width="25%" height="50" />
                <div className="header-menu-right">
                    <ul className="nav navbar-nav navbar-right header-ul">
                        <li className="header-li"><a>ABOUT US</a></li>
                    </ul>
                </div>
            </div>
            <div className="header-shadow"></div>
            <div className="clearfix-div"></div>
            <div className="sign-in-form">
                <form>
                    <h3>Sign In</h3>

                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email" className="form-control" placeholder="Enter email" />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Enter password" />
                    </div>

                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="customCheck1" />
                            <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                        </div>
                    </div>

                    <button type="submit" className="btn btn-primary btn-block">Submit</button>
                    <p className="forgot-password text-right">
                        Forgot <a href="#">password?</a>
                    </p>
                </form>
            </div>
        </div>
    );
}

export default Login;
